public class Lingkaran {
    public double jariJari;
    private double phi = 3.14;
    private double luas;
    public double diameter;

    public double getluas(){
        luas = phi*jariJari*jariJari;
        return luas;
    }

    public double getdiameter(){
        diameter = jariJari*2;
        return diameter;
    }
}