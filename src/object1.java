public class object1 {
    public static void main(String[] args){

        Lingkaran lingkaranKepala = new Lingkaran();
        persegiPanjang persegiPanjangBadan = new persegiPanjang();
        persegiPanjang persegiPanjangTanganKiri = new persegiPanjang();
        persegiPanjang persegiPanjangTanganKanan = new persegiPanjang();
        Lingkaran lingkaranTelapakTanganKiri = new Lingkaran();
        Lingkaran lingkaranTelapakTanganKanan = new Lingkaran();
        persegiPanjang persegiPanjangKakiKiri = new persegiPanjang();
        persegiPanjang persegiPanjangKakiKanan = new persegiPanjang();
        Lingkaran lingkaranTelapakKakiKiri = new Lingkaran();
        Lingkaran lingkaranTelapakKakiKanan = new Lingkaran();

        lingkaranKepala.jariJari = 5;
        persegiPanjangBadan.panjang = 15;
        persegiPanjangBadan.lebar = 12;
        persegiPanjangTanganKiri.panjang = 15;
        persegiPanjangTanganKiri.lebar = 3;
        persegiPanjangTanganKanan.panjang = 15;
        persegiPanjangTanganKanan.lebar = 3;
        lingkaranTelapakTanganKiri.jariJari = 1.5;  
        lingkaranTelapakTanganKanan.jariJari = 1.5;
        persegiPanjangKakiKiri.panjang = 15;
        persegiPanjangKakiKiri.lebar = 5;
        persegiPanjangKakiKanan.panjang = 15;
        persegiPanjangKakiKanan.lebar = 5;
        lingkaranTelapakKakiKiri.jariJari = 2.5;
        lingkaranTelapakKakiKanan.jariJari = 2.5;

        double luasTotal = lingkaranKepala.getluas() + persegiPanjangBadan.getluas()+ persegiPanjangTanganKiri.getluas() + persegiPanjangTanganKanan.getluas() + lingkaranTelapakTanganKiri.getluas() + lingkaranTelapakTanganKanan.getluas() + 
        persegiPanjangKakiKiri.getluas() + persegiPanjangKakiKanan.getluas() + lingkaranTelapakKakiKiri.getluas() + lingkaranTelapakKakiKanan.getluas();
        System.out.println("Luas Total = " + luasTotal);

        double tinggiBadan = lingkaranKepala.getdiameter() + persegiPanjangBadan.lebar + persegiPanjangKakiKiri.panjang + lingkaranTelapakKakiKiri.getdiameter();
        System.out.println("Tinggi Badan = " + tinggiBadan);

    }
}
